﻿using System.Web.Mvc;
using Vidly.Models;


namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies/Random
        public ActionResult Random()
        {
            var movie = new Movie() { Name = "Shrek!" };
            return View(movie);

        }

        public ActionResult Edit(int id)
        {
            return Content("Id=" + id);
        }

        public ActionResult Index(int? page, string sort)
        {
            if (!page.HasValue)
                page = 1;
            if (sort == null)
                sort = "name";
            return Content(string.Format("Page={0}, SortBy={1}", page, sort));
        }
        [Route("movies/released/{year:range(2015,2017)}/{month:regex(\\d{2})}")]
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }

    }

}